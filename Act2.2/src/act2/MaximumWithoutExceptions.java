package act2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MaximumWithoutExceptions {

	public static void main(String[] args) throws IOException {
		try {
		BufferedReader br = new BufferedReader(new FileReader("C:/Users/hbouchoucha/eclipse-workspace/Act2.2/bin/act2/data.txt"));
        // could generate FileNotFoundException (checked)
        int max = -1;
        String line = br.readLine();
        // peut g�n�rer IOException
        while (line != null) {
            int n = Integer.parseInt(line);
            // peut g�n�rer NumberFormatException
            if (n > max) max = n;
            line = br.readLine();
            // peut g�n�rer IOException
        }
        System.out.println("Maximum = " + max);
    }
		
		catch(FileNotFoundException exp) {
		    System.out.println("Le fichier n'existe pas");
		}
		catch(IOException e) {
			System.out.println("Le fichier ne pas etre lu"); 
		} 
		catch(NumberFormatException e) {
			System.out.println("Le fichier ne contient aucune donnee num�rique"); 
		} 
		catch(Exception e) {
			System.out.println(e.getMessage()); 
		    
		} 
	}

	}


